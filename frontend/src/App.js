import './App.css';
import 'react-toastify/dist/ReactToastify.css';
import {Routers} from "./pages/Routers";


function App() {
    return (
        <div className="App">
            <Routers/>
        </div>
    );
}

export default App;
