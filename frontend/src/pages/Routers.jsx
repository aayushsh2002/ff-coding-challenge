import Navbar from "../components/Navbar";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import HomePage from "./HomePage";
import EventCreationPage from "./EventCreationPage";
import {ToastContainer} from "react-toastify";

export const Routers = () => {
    return <BrowserRouter>
        <Navbar/>
        <Routes>
            <Route path='/' element={<HomePage/>}/>
            <Route path='/home' element={<HomePage/>}/>
            <Route path='/event' element={<EventCreationPage/>}/>
        </Routes>
        <ToastContainer/>
    </BrowserRouter>
}