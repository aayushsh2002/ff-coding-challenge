import EventService from "../../services/event-service";
import {toast} from "react-toastify";

export const ActionName = {
    SET_EVENTS: 'setEvents',
    SET_PAGE: 'setPage',
    SET_FILTERS: 'setFilters',
    SET_LOADER: 'setLoader',
    SET_CATEGORIES: 'setCategories'
}

export const fetchEventsAction = (currPage, filter) => dispatch => {
    dispatch({type: ActionName.SET_LOADER, payload: {isLoading: true}})
    EventService.fetchEvents({currPage, ...filter})
        .then(res => dispatch({type: ActionName.SET_EVENTS, payload: res.data.data}))
        .catch(err => toast.error(err.response.data.error.message))
        .finally(() => {
            dispatch({type: ActionName.SET_LOADER, payload: {isLoading: false}})
        })
}

export const fetchCategoriesAction = () => dispatch => {
    EventService.fetchCategories()
        .then(res => {
            dispatch({type: ActionName.SET_CATEGORIES, payload: res.data.data })
        })
        .catch(err => toast.error(err.response.data.error.message))
}

export const setPageAction = (page) => {
    return {
        type: ActionName.SET_PAGE,
        payload: {currPage: page}
    }
}

export const setFiltersAction = (filter) => {
    return {
        type: ActionName.SET_FILTERS,
        payload: {filter, currPage: 1}
    }
}