import {eventReducer} from "./reducers/event-reducer";

import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({event: eventReducer})
export const store = createStore(rootReducer, applyMiddleware(thunk));
