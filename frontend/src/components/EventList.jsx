import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import EventCard from "./EventCard"
import PaginationComp from "./PaginationComp"
import {fetchEventsAction} from "../redux/actions/event-action";

export default function EventList() {

    const {events, currPage, filter} = useSelector(state => state.event);
    const dispatch = useDispatch();


    useEffect(() => {
        dispatch(fetchEventsAction(currPage, filter))
    }, [currPage, filter])

    return (
        <div className='box-container'>
            {events.map((event) => <EventCard event={event} key={event._id}/>)}
            {
                events.length > 0
                    ?
                    <PaginationComp/>
                    :
                    <div style={{fontSize: "22px"}}>
                        No Events found!!!
                    </div>
            }
        </div>
    )
}
