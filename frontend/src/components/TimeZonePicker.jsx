import React from 'react'
import TimezoneSelect from 'react-timezone-select'

 const TimeZonePicker = ({selectedTimezone,setSelectedTimezone}) => {

  return (
    <div>
      <div className="select-wrapper">
        <TimezoneSelect
          value={selectedTimezone}
          onChange={(e)=> {
            setSelectedTimezone(e);
          }}
        />
      </div>
    </div>
  )
}

export default TimeZonePicker;