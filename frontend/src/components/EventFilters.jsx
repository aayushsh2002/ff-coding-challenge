import React, {useState} from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {useDispatch, useSelector} from 'react-redux';
import {setFiltersAction} from '../redux/actions/event-action';


export default function EventFilters() {

    const dispatch = useDispatch();
    const {categories} = useSelector(state => state.event);

    const [title, setTitle] = useState("")
    const [category, setCategory] = useState("")
    const [address, setAddress] = useState("")
    const [isVirtual, setIsVirtual] = useState("");

    const handleFilterSubmit = (e) => {
        e.preventDefault()
        dispatch(setFiltersAction({title, category, address, isVirtual}))
    }

    return (
        <div className='filter-container'>
            <div className='filters'>
                <Form onSubmit={(e)=> e.preventDefault()}>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label style={{fontSize: "25px", fontWeight: "600"}}>Filters</Form.Label>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Control type="text" placeholder="Search event title"
                                      onChange={(e) => setTitle(e.target.value)}/>
                    </Form.Group>
                    <Form.Group style={{display: "flex"}} className="mb-3">
                        <Form.Select aria-label="Default select example" style={{marginTop: ".5rem"}}
                                     onChange={(e) => setCategory(e.target.value)}>
                            <option value={""}>Select event category</option>
                            {
                                categories.map((category, index) => {
                                    return <option value={category} key={index}>{category}</option>
                                })
                            }
                        </Form.Select>
                    </Form.Group>
                    <Form.Group style={{display: "flex"}} className="mb-3">
                        <Form.Select aria-label="Default select example" style={{marginTop: ".5rem"}}
                                     onChange={(e) => setIsVirtual(e.target.value)}>
                            <option value={""}>Select event mode</option>
                            <option value={true}>Virtual</option>
                            <option value={false}>Offline</option>
                        </Form.Select>
                    </Form.Group>
                    <Form.Group className="mb-4" controlId="exampleForm.ControlTextarea1">
                        <Form.Control as="textarea" style={{resize: "none"}} rows={2} placeholder="Search event address"
                                      onChange={(e) => setAddress(e.target.value)}/>
                    </Form.Group>
                    <Form.Group className="mb-1" controlId="exampleForm.ControlTextarea1">
                        <Button variant="primary" onClick={handleFilterSubmit} style={{width:"100%", fontSize:"18px", fontWeight:"600", letterSpacing:"1px"}}>Filter</Button>
                    </Form.Group>
                </Form>
            </div>
        </div>
    )
}
