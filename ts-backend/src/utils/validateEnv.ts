import { cleanEnv, port, str } from 'envalid';

const validateEnv = () => {
  cleanEnv(process.env, {
    NODE_ENV: str(),
    PORT: port(),
    DB_CONNECTION: str(),
    AUTH_KEY: str(),
  });
};

export default validateEnv;
