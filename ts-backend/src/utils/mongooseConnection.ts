import mongoose from "mongoose";
import {DB_CONNECTION} from "@config";

export const mongooseConnection = () => {
  const dbUrl = DB_CONNECTION || ""
  return mongoose.connect(dbUrl).then(() => {
    console.log("connected to db");
  }).catch((err: any) => {
    console.error("Error connecting to db", err)
  })
}
