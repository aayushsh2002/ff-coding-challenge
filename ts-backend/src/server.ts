import App from '@/app';
import {EventsController} from '@controllers/events.controller';
import validateEnv from '@utils/validateEnv';
import {mongooseConnection} from "@utils/mongooseConnection";

validateEnv();
mongooseConnection();
const app = new App([EventsController]);
app.listen();
