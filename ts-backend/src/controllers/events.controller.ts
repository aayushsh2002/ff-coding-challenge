import {Body, Controller, Get, HttpCode, Post, QueryParams, UseBefore} from 'routing-controllers';
import {validationMiddleware} from '@middlewares/validation.middleware';
import EventService from '@/services/events.service';
import {EventSchema, FilterSchema} from '@/joi-validation/event.validation';
import {IEvent, IFilter} from "@interfaces/events.interface";
import authMiddleware from "@middlewares/auth.middleware";

@Controller('/api/event')
@UseBefore(authMiddleware)
export class EventsController {
  public eventService = new EventService();

  @Get('')
  @UseBefore(validationMiddleware(FilterSchema, 'query'))
  async getEvents(@QueryParams() filters: IFilter) {
    const findAllUsersData = await this.eventService.findAll(filters);
    return {data: findAllUsersData}
  }

  @Post('/')
  @HttpCode(201)
  @UseBefore(validationMiddleware(EventSchema, 'body'))
  async createEvent(@Body() event: IEvent) {
    const createEventData: IEvent = await this.eventService.createEvent(event);
    return {data: createEventData._doc};
  }

  @Get('/category')
  async getCategories() {
    const categories: string[] = await this.eventService.findAllCategories();
    return {data: {categories}};
  }

}
