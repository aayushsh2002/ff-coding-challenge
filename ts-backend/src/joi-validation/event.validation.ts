import * as Joi from "joi"

export const EventSchema = Joi.object({
  title: Joi.string().min(5).max(128).required(),
  description: Joi.string().min(10).max(1024).required(),
  category: Joi.string().min(2).max(64).required(),
  date: Joi.date().min("now").required(),
  isVirtual: Joi.boolean(),
  address: Joi.string().min(5).max(1024).when('isVirtual',{is: false, then: Joi.required(), otherwise: Joi.optional().allow("")}),
  endDate: Joi.date().greater(Joi.ref('date')).allow(""),
  selectedTimezone: Joi.object().allow(""),
  conferenceDetails: Joi.string().min(10).when('isVirtual',{is: true, then: Joi.required(), otherwise: Joi.optional().allow("")}),
  imageUrl: Joi.string().allow("").min(10),
  videoUrl: Joi.string().allow("").min(10)
})

export const FilterSchema = Joi.object({
    title: Joi.string(),
    category: Joi.string(),
    isVirtual: Joi.boolean(),
    address: Joi.string(),
    currPage: Joi.number().default(1).min(1)
})

