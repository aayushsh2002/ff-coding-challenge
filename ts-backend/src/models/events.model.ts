import mongoose from "mongoose";

const EventSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    required: true
  },
  endDate: {
    type: Date
  },
  isVirtual: {
    type: Boolean,
    default: true,
  },
  address: {
    type: String
  },
  selectedTimezone: {
    type: Object
  },
  conferenceDetails: {
    type: String
  },
  videoUrl: {
    type: String
  },
  imageUrl: {
    type: String,
    default: "https://www.messagetech.com/wp-content/themes/ml_mti/images/no-image.jpg"
  }
})

export const EventModel = mongoose.model("event", EventSchema);

