## Fig Finance Project

### ts-backend
The backend is built with Node, Express, Typescript and Routing controllers.
To run the backend
```shell
yarn install 
yarn dev
```

### frontend
The frontend is built with React, redux, Hooks
```shell
yarn install
yarn start
```

